#!/usr/bin/env python3

def encrypt(data, key):
    res = []
    for i in range(len(data)):
        res.append(data[i] ^ key[i % 8])
    return bytes(res)


print('Enter a key: ')
key = input()
key = key.encode('ascii')

data = open('data.png', 'rb').read()

encrypted = encrypt(data, key)

fout = open('data.png.encrypted', 'wb')
fout.write(encrypted)
fout.close()
